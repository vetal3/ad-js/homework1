class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  get name() {
    return this._name;
  }
  
  get age() {
    return this._age;
  }
  
  get salary() {
    return this._salary;
  }
  
  set name(name) {
    this._name = name;
  }
  
  set age(age) {
    this._age = age;
  }
  
  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this.lang = lang;
  }

  get salary() {
    return this._salary * 3;
  }
}

const programmer1 = new Programmer("Ivan", 45, 50000, ["JavaScript", "Java"]);
const programmer2 = new Programmer("Andrey", 41, 60000, ["C++", "PHP"]);
  
console.log(programmer1);
console.log(programmer2);